#!/bin/bash

# variables

VMNAME=${1}
VCPU=${2}
RAM=${3}
SSHKEY=$(cat ${4})
CLOUDIMAGEDIR="/home/$USER/cloudimage"
SIZE="20G"

# functions
function checkArgument () {
	if [ $# -lt 4 ]; then
		echo "Usage: $0 <vm name> <vcpu count> <ram count MB> <sshkey path>"
		VMNAME="DefaultVM"
		VCUP="2"
		RAM="2048"
		SSHKEY="/home/$USER/.ssh/id_rsa.pub"
		echo "Default set: ${VMNAME} ${VCPU} ${RAM} ${SSHKEY}"
	fi
}

function checkIFdomainExists () {
	echo "Check if domain exists..."
	virsh --connect qemu:///system dominfo ${VMNAME} > /dev/null 2>&1
	if [ "$?" -eq 0 ]; then
		echo "[WARNING] Domanin ${VMNAME} already exists. End script. Please use different name."
		exit 1
	fi
}

function cloudImageDownload () {
	echo "Start download centos image"
	if [ ! -d "${CLOUDIMAGEDIR}" ]; then
		mkdir -p ${CLOUDIMAGEDIR}
	fi
	wget -O ${CLOUDIMAGEDIR}/centos.img https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2 
}

function resizeCloudImage () {
	echo "Resize cloud image"
	qemu-img resize ${CLOUDIMAGEDIR}/centos.img ${SIZE}
}

function createVolume () {
	echo "Create Volume"
	virsh --connect qemu:///system vol-create-as default ${VMNAME} ${SIZE} --format qcow2
}

function uploadVolume () {
	echo "Upload Volume"
	virsh --connect qemu:///system vol-upload --pool default ${VMNAME} ${CLOUDIMAGEDIR}/centos.img
}

function createUserDataFile () {
	echo "create user-data file for ISO"
	cat > ${CLOUDIMAGEDIR}/user-data << EOF
#cloud-config

preserve_hostname: False
hostname: $VMNAME
fqdn: $VMNAME.example.local

runcmd:
  - [ yum, -y, remove, cloud-init ]

output: 
  all: ">> /var/log/cloud-init.log"

ssh_svcname: ssh
ssh_deletekeys: True
ssh_genkeytypes: ['rsa', 'ecdsa']

ssh_authorized_keys:
  - ${SSHKEY}
EOF
}

function createMetaDataFile () {
	echo "Create meta-data file"
	echo "instance-id: ${VMNAME}; local-hostname: ${VMNAME}" > ${CLOUDIMAGEDIR}/meta-data
}

function createCDROM () {
	echo "Start Create CDROM"
	genisoimage -output ${CLOUDIMAGEDIR}/cidisk.iso -volid cidata -joliet -r ${CLOUDIMAGEDIR}/user-data ${CLOUDIMAGEDIR}/meta-data
}

function createVirtualMachine () {
	echo "Create VM"
	virt-install --connect qemu:///system \
		--import --name ${VMNAME} --ram ${RAM} --vcpu ${VCPU} \
		--disk vol=default/${VMNAME} --disk ${CLOUDIMAGEDIR}/cidisk.iso,device=cdrom \
		--network bridge=virbr0,model=virtio --os-variant=rhel7 --os-type=linux \
		--noautoconsole --graphics spice
}

function getIPAddr () {
	echo "Get IP address from new VM"
	MAC=$(virsh --connect qemu:///system dumpxml ${VMNAME} | awk -F\' '/mac address/ {print $2}')
	while true; do
		IP=$(grep -B1 $MAC /var/lib/libvirt/dnsmasq/virbr0.status | head -n 1 | awk '{print $2}' | sed -e s/\"//g -e s/,//)
		if [ "${IP}" = "" ]; then
			sleep 1
		else
			break
		fi
	done
}

function ejectCDROM () {
	echo "Eject CDROM"
	virsh --connect qemu:///system change-media ${VMNAME} hda --eject --config
	rm -rf ${CLOUDIMAGEDIR}/user-data ${CLOUDIMAGEDIR}/cidisk.iso ${CLOUDIMAGEDIR}/centos.img ${CLOUDIMAGEDIR}/meta-data
}

function showIPAddr () {
	sleep 30
	echo "${VMNAME} IP: ___  ${IP}  ___"
}


# MAIN BASH -------------------------------------------------------------------------------------------------------------------------------------------------

checkArgument ${VMNAME} ${VCUP} ${RAM} ${SSHKEY}
checkIFdomainExists
cloudImageDownload
createUserDataFile
createMetaDataFile
createCDROM
resizeCloudImage
createVolume
uploadVolume
createVirtualMachine
getIPAddr
ejectCDROM
showIPAddr
